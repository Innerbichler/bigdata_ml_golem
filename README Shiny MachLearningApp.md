# Machine Learning Shiny App 

Die App erzeugt im Frontend nach Aufruf der Adresse 
https://runisfun.shinyapps.io/machlearn/

folgende Oberfläche:

![](AppFirst.png)

Bei Klick auf "Browse" kann man ein beliebiges EXCEL file laden, das zumindest 2 numerische Spalten enthalten muss. Folgendes Worksheet wird als Beispile verwendet (TestDat.xlsx):

![](EXCEL.png)

Das WS kann fehlende Werte enthalten; wenn CHAR und NUM gemischt sind (z.B. "NA") muüssen die CHAR Felder gelöscht werden.
Nach Ladung können das WorkSheet, die Spalte mit der Region und die Spalte mit dem Output (y-variable) gewählt werden. Die Spalte "Region" hat keine Bedeutung für das ML, allerdings enthalten alle WS in der Firma eine kategorische Spalte, die von der Berechnung ausgeschlossen wird.

![](SidePanel.png)

Bei Klick auf "Calculate" wird der Correlations Plot erzeugt: er liefert den Pearson's correlations coefficient als Barchart.
Als Nächstes müssen die x-Variablen bestimmt werden. Es können alle verwendet werden, oder nur einige. Der Correlations Plot kann dabei eine Hilfe sein.

<img src="Xvar.png" width="200" height="600" />


![](Hist.png)

Die Info Boxes unter dem Histogramm geben aus, wieviele Variablen keine oder nur geringe Varianz haben. Diese werden nicht in der Auswertung verwendet. 
Der Algorithmus zum Auffüllen der fehlenden Werte kann unterhalb der X-Variablen ausgewählt werden, ebenso das scaling.

**Drücken auf den Reiter "Models" startet die Machine Learning Berechnungen und gibt folgende Ergebnisse aus:** 

Die Daten werden in ein Train und ein Test Datenset geteilt. Das Modelling passiert nur am Train Dataset, der RMSEP wird mit dem Test-Datensatz berechnet.


### Plot observed vs predicted
![](ObsPred.png)
### Variable Importance vom Random Forest
![](RF_VImp.png)
### Tabelle der Modelle, bestes Modell nach RMSECV und RMSE prediction
![](Tabellen.png)
### Graphik des NNs
![](NNetPlot.png)
### Variable Importance des NNs
![](NNetVimp.png)


# GOLEM

### Erfahrungen und Probleme mit dem package builder GOLEM

Nach Installation des Golem packages kann in RStudio unter "New Project", "new directory" ein Golem Projekt angelegt werden:

![](NewGolemProj.png)

Nach Bestimmung des Foldernamens wird die Verzechnisstruktur erstellt:


Golem teilt die nützlichen Codes in 4 Teile auf: dev/01_start.R, dev/02_dev.R, dev/03_deploy.R und dev/run_dev.R
Als erstes wird 01_start.R interpretiert, sodass die Hilfsfiles (z.B. .gitignore, .Rbuildignore, CODE_OF_CONDUCT.md, LICENSE.md, NAMESPACE, README.Rmd) generiert werden. Das Golemverzeichnis sieht bereits so aus wie ein R CRAN package.

![](GolemFolder.png)

Der "R" Folder enhält die Files: app_ui.R, app_server.R und app_run.R. Es wird geraten, den eigentlichen Shiny R code auf die beiden Files UI und SERVER aufzuteilen. Hier ist der erste Fehler passiert: In Golem, und wohl allen packages, die auf CRAN gespeichert werden, muss der import der benötigten Pakete mit dem Befehl #' @import erfolgen, "library" funktioniert nicht:

![](RimportPack.png)

Der "import" Befehl dürfte an Python angelehnt sein, weil auch "import from" möglich ist, das ein selektives Einlesen von einzelnen Funktionen ermöglicht. 


Die Machine Learning App öffnet wie erwartet. Nach Einlesen der .xlsx Datei können die Gruppenspalte und die y-Spalte ausgewählt werden. Nach klick auf "calculate" bekommt man leider keinen Correlations plot:

![](Fehler1.png)

Lösung: kein Golem-bedingter Fehler, hatte eine Funktion versehentlich gelöscht.

### Building the R package "ML golem"

In 03_deploy.R kann das package nach R-Vorgaben getestet werden:
````R
# Deploy a Prod-Ready, Robust Shiny Application.
# 
# 4. Test my package

devtools::test()
rhub::check_for_cran()
````
Ergebnis (erwartet): 

![](MLgolemWarning1.png)
![](MLgolemWarning2.png)

Golem liefert mehrere Möglichkeiten das package zu deployen:
````R
# 5. Deployment elements

## 5.1 If you want to deploy on RStudio related platforms
golem::add_rstudioconnect_file()
golem::add_shinyappsio_file()
golem::add_shinyserver_file()

## 5.2 If you want to deploy via a generic Dockerfile
golem::add_dockerfile()

## 5.2 If you want to deploy to ShinyProxy
golem::add_dockerfile_shinyproxy()

## 5.2 If you want to deploy to Heroku
golem::add_dockerfile_heroku()
````

(Deployen hab ich nicht mehr ausprobiert, da ich bereits eine deployte Version habe: 
https://gitlab.com/Innerbichler/bigdata_ml_golem
)

## Fazit:

Das Generieren einer "production-ready R-extention" ist prinzipiell relativ einfach mit dem Golem package. Im Detail allerdings bedeutet es zusätzliche Arbeit (berechtigterweise), weil die Tests zu schreiben sind, die Funktionsnamen anzupassen sind, .git- und .Rbuildignore file, NAMESPACE, README und LICENSE files zu verwalten sind. Im Sinne von good programming practice sollte es immer gemacht werden. 